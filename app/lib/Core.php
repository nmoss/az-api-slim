<?php

namespace lib;

use lib\Config;
use PDO;

class Core
{
    public $dbh;  //handle for the database connection
    private static $instance;

    private function __construct()
    {
        $this->dbh = Config::read('pdo');
    }

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            $object = __CLASS__;
            self::$instance = new $object;
        }
        return self::$instance;
    }

    public static function stripSlashesDeep(&$value)
    {
        if (is_array($value)) {
            $value = array_map(['lib\Core', 'stripSlashesDeep'], $value);
        } else if (is_object($value)) {
            $vars = get_object_vars($value);
            foreach ($vars as $key => $data) {
                $value->{$key} = Core::stripSlashesDeep($data);
            }
        } else if (is_string($value)) {
            $value = stripslashes($value);
        }
        return $value;
    }

    public static function output(&$app, &$data)
    {
        $data = self::stripSlashesDeep($data);
        $request = $app->request();
        $params = $request->params();
        if (array_key_exists('pretty', $params)) {
            return json_encode($data, JSON_PRETTY_PRINT);
        }
        return json_encode($data);
    }

    public static function extractPostData(&$app)
    {
        $post_data = json_decode($app->request->getBody(), true);
        if ($post_data === null) {
            $app->halt(400, json_encode(["error" => "bad_request",
                "error_description" => "malformed post data"]));
        }
        error_log("POST DATA HERE : ** " . print_r($post_data, true));
        return $post_data;
    }

    public static function updateWrapFunc($term)
    {
        $safe_term = str_replace(array('-'), array('_'), $term);
        return "`$term`=:$safe_term";
    }

    public static function quoteWrapFunc($term)
    {
        return "`$term`";
    }

    public static function colonAddFunc($term)
    {
        $term = str_replace(array('-'), array('_'), $term);
        return ":$term";
    }
    public static function unwrapFilterFunc(&$filterArray)
    {
        if(empty($filterArray)) {
            return [];
        }
        $actualFilter = [];
        $filterParts = explode(',', $filterArray);
        foreach( $filterParts as $filterPart) {
            $keyValuePart = explode(':', $filterPart);
            $key=$keyValuePart[0];
            $value = $keyValuePart[1];
            $actualFilter[$key] = $value;

        }
    return $actualFilter;
    }
}
