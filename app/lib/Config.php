<?php

namespace lib;

class Config
{
    public static $conf_array;

    public static function read($name)
    {
        return self::$conf_array[$name];
    }

    public static function write($name, $value)
    {
        self::$conf_array[$name] = $value;
    }
}
