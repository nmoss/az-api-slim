<?php

namespace models;

use lib\Core;
use lib\Config;
use PDO;

class Show
{
    protected $core;

    public function __construct()
    {
        $this->core = Core::getInstance();
    }

    public function all()
    {
        $sql = "
            SELECT * FROM `audio_shows` shows
            INNER JOIN 
                (`audio_stations` stations)
                ON (
                    `stations`.`stationid` = `shows`.`stationid`
                )
            WHERE 
                `shows`.`inactive` = 0
            ORDER BY
                `shows`.`stationid`,
                `shows`.`rank` DESC,
                `shows`.`alphaname`
        ";
        $stmt = $this->core->dbh->prepare($sql);
        if ($stmt->execute()) {
            $r = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $r = false;
        }
        return $r;
    }

    public function allFromStation($stationId)
    {
        $sql = 'SELECT * FROM `audio_shows` WHERE `stationid` = :stationId AND `inactive` = 0 ORDER BY `rank` DESC, `alphaname`';
        $stmt = $this->core->dbh->prepare($sql);
        $stmt->bindParam(':stationId', $stationId);
        if ($stmt->execute()) {
            $r = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $r = false;
        }
        return $r;
    }

    public function oneFromStation($stationId, $showId)
    {
        $sql = 'SELECT * FROM `audio_shows` WHERE `stationid` = :stationId AND `showid` = :showId';
        $stmt = $this->core->dbh->prepare($sql);
        $stmt->bindParam(':stationId', $stationId);
        $stmt->bindParam(':showId', $showId);
        if ($stmt->execute()) {
            $r = $stmt->fetch(PDO::FETCH_ASSOC);
        } else {
            $r = false;
        }

        return $r;
    }

    public function oneFromShowId($showId)
    {
        $sql = 'SELECT * FROM `audio_shows` WHERE `showid` = :showid';
        $stmt = $this->core->dbh->prepare($sql);
        $stmt->bindParam(':showid', $showId);
        if ($stmt->execute()) {
            $r = $stmt->fetch(PDO::FETCH_ASSOC);
        } else {
            $r = false;
        }

        return $r;
    }

    /*
    
    probably not needed.  Same as allFromStation just using a different DB table. will consider changing.
     
    public function List($stationId) 
    {
        $sql = "
        SELECT * FROM tblshows 
        WHERE inactive=1
            AND fkStationID=? 
            AND rank!=0 
            AND pkShowID!=1 
        ORDER BY rank
        ";

        $stmt = $this->core->dbh->prepare($sql);
        $stmt-> bindParam(':stationId', $stationId);
    }
    */
}
