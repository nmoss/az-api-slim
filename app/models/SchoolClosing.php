<?php

//TODO: Fix sort in category GET to operate like filter with category header exception.


namespace models;

use lib\Core;
use lib\Config;
use PDO;

class SchoolClosing
{
    protected $core;

    public function __construct()
    {
        $this->core = Core::getInstance();
    }

    public function validateCreateData(&$createData)
    {
        if(!is_array($createData)){
            return false;
        }
        $required_keys = ['category'];
        foreach ($required_keys as $key) {
            if (!array_key_exists($key, $createData) || $createData[$key] === "") {
                return false;
            }
        }
        return true;
    }
    //All Columns of table
    public function objectColumns() 
    {
        return [
            'id',
            'orgid',
            'category',
            'effective_date',
            'last_update',
            'updated',
            'orgname',
            'schoolrelated',
            'detail',
            'lastupdated',
            'active'
        ];

    }
    //Modifiable by PUT
    public function modifiableColumns() 
    {
        return [
            'category',
            'effective_date',
            'last_update',
            'updated',
            'orgname',
            'schoolrelated',
            'detail',
            'lastupdated',
            'active'
        ];
    }

    /**
     * Creates a value map for a school closing so that a sql query string can be made more easily.
     * @param  array  &$keys_to_update An array of authenticated keys.
     * @param  array  &$data           Post/Put data
     * @param  integer $id             (optional) Identification number for a given school closing.
     * @return array                   A value map that is ready to be inserted into a query string.
     */
    public function createValueMap(&$keys_to_update, &$data, $id = 0) {

        if($id !== 0){
            $value_map = [':id' => $id];
        } else {
            $value_map = [];  
        }
        foreach ($keys_to_update as $key) {
            $value_map[":$key"] = $data [$key];
        }
        return $value_map;
    }

    /**
     * Adds a parameter to a value map by reference .
     * @param array &$value_map Existing value map
     * @param array &$termArray Array of terms to be added.
     */
    public function addToValueMap(&$value_map, &$termArray) {
        foreach ($termArray as $term => $value) {
            $value_map[":$term"] = $value;
        }
    }

    /**
     * Creates an update value map for returning updates.
     * @param  array  &$keys_to_update An array of authenticated keys.
     * @param  array  &$data           Post/Put data
     * @param  integer $id             Identification number for a given school closing.
     * @return array                   A value map that is ready to be inserted into a query string.
     */
    public function createUpdateReturnMap(&$keys_to_update, &$data, $id) {
        $value_map = ['id' => $id];
        foreach ($keys_to_update as $key) {
            $value_map[$key] = $data[$key];
        }
        return $value_map;
    }
     
    /**
     * Retreives a school closing accoriding to the given ID.
     * @param  int    $id   Identification number for a given school closing.
     * @return array        Array of school closing data.
     */
    public function getById($id) 
    {
        $r = array();
        $sql = 'SELECT * FROM `schoolclosings_new` WHERE `id` = :id';
        $stmt = $this->core->dbh->prepare($sql);
        $stmt->bindParam(':id', $id);

        if($stmt->execute()) {
            $r = $stmt->fetch(PDO::FETCH_ASSOC);
        } else {
            $r = false;
        }
        return $r;
    }

    //This will be the main function in which you can pass a filter declaring whether you want to update or not.
    /**
     * Requests all active school closings according to the given
     * @param  array  $filter An array of passed in filter options.  
     *                Filter Format: filter=<key1>:<value1>,<key2>:<value2>
     *                EX. http://example.com/schoolclosing?filter=id:1234,category:browncow,active:0
     *                
     *                        expired:<any value>     Passing 'expired' as a key (no matter the value) will return any school 
     *                                                closings that have not been updated in over 5 minutes and thus need to be 
     *                                                updated.
     * @param  array  $sort   An array of passed in sorting options.
     *                Sort Format: sort=<key1>:<value1>,<key2>:<value2>
     *                EX. http://example.com/schoolclosing?sort=id:1234,category:browncow,active:0
     *
     *                        category:<any value>   Passing 'category' as a key will sort the school closings by 
     *                                               category groups.
     *                        desc    :<any value>   Passing the abbreviation 'desc' will sort them in descending order.
     *                                               (Ascending by default)
     *                                               
     * @return array          Dependant on the filter & sort options, Default: returns an array of active and inactive schoolclosing data.
     *                                  *Notes: 
     */
    public function getByFilter($filter=[], $sort=[])
    {
        $valid_keys = $this->objectColumns();
        
        $data_keys = array_keys($filter);
      
        $keys_to_update = array_intersect($data_keys, $valid_keys);



        $value_map = $this->createValueMap($keys_to_update, $filter);
      
        $placedTheWhere = false;
        $sql = "
            SELECT * FROM `schoolclosings_new` schools ";
        //Filtering
        if (!empty($keys_to_update)) {
            $sql .= "
            WHERE ";
            $placedTheWhere = true;
            $sql .= implode(' AND ', array_map(['lib\Core', 'updateWrapFunc'], $keys_to_update)) . " ";
        }
        if (array_key_exists('expired', $filter)) {
            $sql .= ($placedTheWhere) ? " AND " : " WHERE ";

            $filter['expired'] = date('Y-m-d h:m:s');
            $sql .= "TIMESTAMPDIFF(MINUTE, `schools`.`last_update` , :expired ) > 5 
            ";
        }
        //Sorting
        if (!empty($sort)) {
            $sql .= "
            ORDER BY ";
        
            if (array_key_exists('category', $sort) || array_key_exists('catheaders', $sort)) {
                $sql .= "`schools`.`category` ";
            }
            if (array_key_exists('desc', $sort)) {
                $sql .= "
                DESC ";
            } else {
                $sql .= "
                ASC ";
            } 
        }

        $stmt = $this->core->dbh->prepare($sql);
        if(array_key_exists('expired', $filter)){
            $termsToAdd = [];
            $termsToAdd['expired'] = $filter['expired'];
            $this->addToValueMap($value_map, $termsToAdd);
        }
        if(!empty($value_map)) {
            $condition = $stmt->execute($value_map);
        }
        else {
            $condition = $stmt->execute();
        }
        if($condition) {
            $r = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $r = [];
        }
        return $r;
    }

    /**
     * Updates a school closing according to its given ID and data.
     * @param  int   $id    The identification number for the desired school closing.
     * @param  array &$data An array of acceptable data options to update the data.  
     * @return array        An array containing all the updated school closings.
     */
    public function updateDataById($id, &$data)
    {
        $valid_keys = $this->modifiableColumns();
        $data_keys = array_keys($data);
        $keys_to_update = array_intersect($data_keys, $valid_keys);
        $value_map = $this->createValueMap($keys_to_update, $data, $id);
    
        $sql = 'UPDATE `schoolclosings_new` SET  ' . 
            
        implode(',', array_map(['lib\Core', 'updateWrapFunc'], $keys_to_update)) .
        ' WHERE  id = :id';

        $stmt = $this->core->dbh->prepare($sql);
        if ( $stmt->execute($value_map)) {
            $r = $this->createUpdateReturnMap($keys_to_update, $data, $id);
        } else {
            $r = false;
        }
        return $r;
    }

    /**
     * Prior to updating the table, this method will make all the school closings in
     * the table 'active' = 0.  Setting them as inactive.  
     * @return array  An array of all previously activated school closings.
     */
    public function deactivateAll()
    {
        $sql = 'UPDATE `schoolclosings_new` SET `active` = 0 WHERE `active` = 1';

        $stmt = $this->core->dbh->prepare($sql);
        if($stmt->execute()) {
            $r = $stmt->fetch(PDO::FETCH_ASSOC);
        } else {
            $r = false;
        }
        return $r;
    }

    /**
     * Creates a new school closing with given post data.
     * @param  array &$createData The post data provided for the school closing.
     * @return array              The data for the created post.
     */
    public function create(&$createData) 
    {
        $modifiableColumns = $this->modifiableColumns();
        $data_keys = array_keys($createData);
        $keys_to_update = array_intersect($data_keys, $modifiableColumns);

        foreach ($createData as $key => $value) {
            if (in_array($key, $modifiableColumns)) {
                $values[":{$key}"] = $value;
            }
        }

        $sql = 'INSERT INTO `schoolclosings_new` ( ' .
            implode(',' , array_map(['lib\Core', 'quoteWrapFunc'], $keys_to_update)) . 
            ') VALUES (' .
            implode(',', array_map(['lib\Core', 'colonAddFunc'], $keys_to_update)) .
            ')';

        $stmt=$this->core->dbh->prepare($sql);
        if($stmt->execute($values)) {
            $id = $this->core->dbh->lastInsertId();
            return $this->getById($id);
       }
   }

   /**
    * Deletes the school closing by ID
    * @param  int $id    The identification number of the school closing
    * @return array      An array of the data for the deleted school closing.
    */
   public function deleteById($id) 
   {
        $sql = 'DELETE FROM `schoolclosings_new` WHERE `id` = :id';
        $stmt = $this->core->dbh->prepare($sql);
        $stmt->bindParam(':id', $id);
        return $stmt->execute();
   }
}