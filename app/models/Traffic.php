<?php

namespace models;

use lib\Core;
use lib\Config;
use PDO;

class Traffic
{
    protected $core;

    public function __construct()
    {
        $this->core = Core::getInstance();
    }

    public function all()
    {
        return false;
    }

    public function allbyLocation($lon, $lat, $distance)
    {
        $sql = "SELECT *, ((3958*3.1415926*sqrt((lat-:lat)*(lat-:lat) + cos(lat/57.29578)*cos(:lat/57.29578)*(lon-:lon)*(lon-:lon))/180)) as vdistance  FROM traffic_cams WHERE ((3958*3.1415926*sqrt((lat-:lat)*(lat-:lat) + cos(lat/57.29578)*cos(:lat/57.29578)*(lon-:lon)*(lon-:lon))/180)) < :distance";

        $stmt = $this->core->dbh->prepare($sql);
        $stmt->bindParam(':lat', $lat);
        $stmt->bindParam(':lon', $lon);
        $stmt->bindParam(':distance', $distance);
        if ($stmt->execute()) {
            $r = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $r = false;
        }
        return $r;
    }

}
