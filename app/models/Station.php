<?php

namespace models;

use lib\Core;
use lib\Config;
use PDO;

class Station
{
    protected $core;

    public function __construct()
    {
        $this->core = Core::getInstance();
    }

    public function all()
    {
        $sql = 'SELECT * FROM `audio_stations`';
        $stmt = $this->core->dbh->prepare($sql);
        if ($stmt->execute()) {
            $r = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $r = false;
        }
        return $r;
    }

    public function one($stationId)
    {
        $sql = 'SELECT * FROM `audio_stations` WHERE stationid = :stationId';
        $stmt = $this->core->dbh->prepare($sql);
        $stmt->bindParam(':stationId', $stationId);
        if ($stmt->execute()) {
            $r = $stmt->fetch(PDO::FETCH_ASSOC);
        } else {
            $r = false;
        }

        return $r;
    }
}
