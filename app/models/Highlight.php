<?php

namespace models;

use lib\Core;
use lib\Config;
use PDO;

class Highlight
{
    protected $core;

    public function __construct()
    {
        $this->core = Core::getInstance();
    }

    public function all($date, $stationID = 0)
    {
        error_log("date = " . $date);

        $sql = "
            SELECT * FROM `audio` highlights
            INNER JOIN `audio_shows` shows
            ON `shows`.`showid` = `highlights`.`showid`
            WHERE `highlights`.`clip` = 1 ";
            $sql .= ($date != 0) ?  "AND DATE(`highlights`.`datepublished`) = :date" : "AND `highlights`.`datepublished` 
                                                                                        IN (SELECT MAX(`datepublished`) 
                                                                                        FROM `audio`)";
        $sql .= ($stationID != 0) ? " AND `shows`.`stationid` = :stationid" : "";
        $sql .= "
            ORDER BY `highlights`.`datepublished`
            DESC
            ";

        error_log("SQL = " . print_r($sql, true));
        $stmt = $this->core->dbh->prepare($sql);
        if($date != 0) {
            $stmt->bindParam(':date', $date);
        }
        if($stationID != 0) {
            $stmt->bindParam(':stationid', $stationID);
        }
        if ($stmt->execute()) {
            $r = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $r = false;
        }
        return $r;
    }


    public function lastDatePublished() {
        $sql = 'SELECT MAX( DATE(`datepublished`)) AS last_date_published
                FROM `audio`
                WHERE 1 
                LIMIT 1';
        $stmt = $this->core->dbh->prepare($sql);
        if ($stmt->execute()) {
            $r = $stmt->fetch(PDO::FETCH_ASSOC);
        } else {
            $r = false;
        }
        return $r;
    }
}
