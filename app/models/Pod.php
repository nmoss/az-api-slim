<?php

namespace models;

use lib\Core;
use lib\Config;
use PDO;

class Pod
{
    protected $core;

	public function __construct()
	{
		$this->core = Core::getInstance();
	}

	public function all()
	{
		$sql = 'SELECT * FROM `audio`';
		$stmt = $this->core->dbh->prepare($sql);
		if ($stmt->execute()) {
			$r = $stmt->fetchAll(PDO::FETCH_ASSOC);
		} else {
			$r = false;
		}
		return $r;
	}

    public function oneFromAid($aid) 
    {
        $sql = " 
            SELECT * FROM `audio` pods
            INNER JOIN 
                (`audio_shows` shows)
                ON (
                    `shows`.`showid` = `pods`.`showid`
                )
            WHERE
                `pods`.`audioid`= :aid
                AND `pods`.`inactive` = 0
            LIMIT 1
        ";

        $stmt = $this->core->dbh->prepare($sql);
        $stmt->bindParam(':aid', $aid);
        if ($stmt->execute()) {
            $r = $stmt->fetch(PDO::FETCH_ASSOC);
        } else {
            $r = false;
        }

        return $r;

    }


	public function allFromShow($showId, $since, $filter = [])
	{
        $sql = "
            SELECT * FROM `audio` pods
            INNER JOIN 
                (`audio_shows` shows)
                ON (
                	`shows`.`showid` = `pods`.`showid`
                )
			WHERE
			    `pods`.`inactive` = 0 
			    AND `pods`.`showid` = :showId
       	";
       	//
        //Completes Filter Implimentation Here.
        //filter='date': YYYY-MM-DD , 'ignoreAid': 00 , 'clip'=1 , 'asc'=1
        
        //Date
        if (array_key_exists('date', $filter)) {
         	$sql .= " AND date_format(`pods`.`datepublished`, '%Y-%m-%d') = :date
         	";
        }
        //Ignored Audio ID : passed in audio_id to be ignored.
        if (array_key_exists('ignore_aid', $filter)) {

         	$sql .= " AND `pods`.`audioid` != :ignore_aid 
         	";
        }
        //Clip : whether or not it is a clip
        if (array_key_exists('clip', $filter)) {
         	$sql .= " AND `pods`.`clip` = :clip
         	";
        }
        //Default Order By Date Published: Ordering by audio pods 'datepublished'
         	$sql .= "
			ORDER BY 
				`pods`.`datepublished` DESC, `pods`.`title` ASC
			";
		//Ascending Order
		//Note: asc is always 1 if it exists in filter.
        /*
		if (array_key_exists('asc', $filter)) {
         	$sql .= " ASC 
         	";
        } else {
        //Default Descending Order
			$sql .= " DESC 
			";
		}
        */
			//Paging Param:  'since'
			$sql .= " LIMIT :since,20";
        

        $stmt = $this->core->dbh->prepare($sql);
        $stmt->bindParam(':showId', $showId);
        $stmt->bindParam(':since', $since, PDO::PARAM_INT);
        if (array_key_exists('date', $filter)) {
        	$stmt->bindParam(':date', $filter['date']);
        }
        if (array_key_exists('ignore_aid', $filter)) {
        	$stmt->bindParam(':ignore_aid', $filter['ignore_aid'], PDO::PARAM_INT);
        }
        if (array_key_exists('clip', $filter)) {
            $stmt->bindParam(':clip', $filter['clip'], PDO::PARAM_INT);
        }
        if ($stmt->execute()) {
            $r = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $r = false;
        }

        return $r;
    }
}
