<?php


//Load the base config variables
require __DIR__ . '/config.php';


// Set the Content-Type header
$app->contentType('application/json');

/**************************************************************************
* Routes
**************************************************************************/
require __DIR__ . '/routes/errors.php';
require __DIR__ . '/routes/stations.php';
require __DIR__ . '/routes/shows.php';
require __DIR__ . '/routes/pods.php';
require __DIR__ . '/routes/highlights.php';

require __DIR__ . '/routes/traffic.php';

require __DIR__ . '/routes/schoolclosings.php';

$app->get('/hello/:name', function($name) {
    echo "Hello, $name";
});
