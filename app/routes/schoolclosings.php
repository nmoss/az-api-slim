<?php

use lib\Core;
use models\SchoolClosing;

/***************************************************************************
 * BY CATEGORY
 ***************************************************************************/

/**
 * GET by category; Returns a list of school closings according to the passed in 
 * filter or sort array options. Note* See Model comments for more information.
 */
$app->get('/schoolclosings/', function() use ($app) {

	$request = $app->request();
	$params = $request-> params();

	$actualFilter = [];
	if (array_key_exists('filter', $params)){
		$filter = $params['filter'];
		$actualFilter = Core::unwrapFilterFunc($filter);
	}
	$actualSort = [];
	if (array_key_exists('sort', $params)) {
		$sort = $params['sort'];
		$actualSort = Core::unwrapFilterFunc($sort);
	}
	$schoolClosingModel = new SchoolClosing();
	$schoolClosings = $schoolClosingModel->getByFilter($actualFilter, $actualSort);

	//Sorting by category headers for page display.
	if(array_key_exists('catheaders', $actualSort)){
		$catGroups = [];
		foreach($schoolClosings as $closing) {
			$catName = $closing['category'];
			$catGroups[$catName]['closings'][] = $closing;
		};

		$output = [];
		foreach($catGroups as $catName => $catGroup) {
			$closings = $catGroup['closings'];

			$output[] = [
				'category' => $catName,
				'closings' => $closings
			];
		}
		echo Core:: output($app, $output);
	} else {

    echo Core::output($app, $schoolClosings);
	}
});

/**
 * PUT by category; updates the school closings from XML. - not functional yet.
 */
$app->put('/schoolclosings/', function() use ($app) {

	$schoolClosingModel = new SchoolClosing();
	$postData = Core::extractPostData($app);
	$result = $schoolClosingModel->updateAll($postData);
	echo Core::output($app, $result);
});

/**
 * POST by category; creates a new school closing.
 */
$app->post('/schoolclosings/', function() use ($app) {

	$postData = Core::extractPostData($app);
	$schoolClosingModel = new SchoolClosing();
	if(!$schoolClosingModel->validateCreateData($postData)) {
		$out = ["error" => "bad_request",
		"error_description" => "missing required parameters" ];
		$app->halt(400, Core::output($app, $out));
	}
	$schoolClosing = $schoolClosingModel->create($postData);
	echo Core::output($app, $schoolClosing);
});

/***************************************************************************
 * BY ID
 ***************************************************************************/

/**
 * GET by ID; retreives a school closing according to its given ID#.
 */
$app->get('/schoolclosings/:id/', function($id) use ($app) {
	$schoolClosingModel = new SchoolClosing();
	$schoolClosing = $schoolClosingModel->getById($id);
	echo Core::output( $app, $schoolClosing );
});

/**
 * PUT by ID; updates the school closing identified by the given ID#.
 */
$app->put('/schoolclosings/:id/', function($id) use ($app) {

	$schoolClosingModel = new SchoolClosing();

	$schoolClosing = $schoolClosingModel->getById($id);
	if ($schoolClosing === false) {
		$out = ["error" => "not_found",
		"error_description" => "could not find a record for school closing {$id}" ];
		$app->halt(404, Core::output($app, $out));
	}
	$postData = Core::extractPostData($app);
	$schoolClosingModel->updateDataById($schoolClosing['id'], $postData);
	$schoolClosing = $schoolClosingModel->getById($schoolClosing['id']);
	echo Core::output($app, $schoolClosing);
});

/**
 * DELETE by ID; deletes the school closing identified by the given ID#.
 */
$app->delete('/schoolclosings/:id/', function($id) use ($app) {

	$schoolClosingModel = new SchoolClosing();
	$schoolClosing = $schoolClosingModel->getById($id);
	if(!$schoolClosing) {
		$out = ["error" => "not_found",
			"error_description" => 
				"could not find a record for school closing : {$id}"];
		$app->halt(404, Core::output($app, $out));
	}

	$result = $schoolClosingModel->deleteById($schoolClosing['id']) . '';
	Core::output($app, $result);
});
