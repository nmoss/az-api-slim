<?php

use lib\Core;
use models\Station;

$app->get('/stations/', function() use ($app) {
    $stationModel = new Station();

    $stations = $stationModel->all();
    echo Core::output($app, $stations);
});

$app->get('/stations/:stationId', function($stationId) use ($app) {
    $stationModel = new Station();

    $station = $stationModel->one($stationId);
    if ($station === false) {
        $output = [
            'error' => 'not_found',
            'error_description' => "could not find station with id {$stationId}"
        ];
        $app->halt(404, Core::output($app, $output));
    }
    echo Core::output($app, $station);
});
