<?php

use lib\Core;
use models\Show;

$app->get('/shows', function() use ($app) {
    $showModel = new Show();
    $shows = $showModel->all();
    
    $stationGroups = [];
    foreach ($shows as $show) {
        $stationId = $show['stationid'];
        $stationName = $show['stationname'];
        unset($show['stationid']);
        unset($show['stationname']);
        unset($show['oldstationid']);

        if (!array_key_exists($stationId, $stationGroups)) {
            $stationGroups[$stationId]['name'] = $stationName;
        }

        $stationGroups[$stationId]['shows'][] = $show;
    };

    $output = [];
    foreach ($stationGroups as $stationId => $stationGroup) {
        $stationName = $stationGroup['name'];
        $shows = $stationGroup['shows'];
        $output[] = [
            'stationid' => $stationId,
            'stationname' => $stationName,
            'shows' => $shows
        ];
    }

    echo Core::output($app, $output);
});



$app->get('/stations/:stationId/shows', function($stationId) use ($app) {
    $showModel = new Show();

    $shows = $showModel->allFromStation($stationId);
    $output[] = [
        'stationid' => $stationId,
        'stationname' => '',
        'shows' => $shows
    ];
    echo Core::output($app, $output);
});

$app->get('/stations/:stationId/shows/:showId', function($stationId, $showId) use ($app) {
    $showModel = new Show();

    $show = $showModel->oneFromStation($stationId, $showId);
    if ($show === false) {
        $output = [
            'error' => 'not_found',
            'error_description' => "could not find show with id {$showId} and station id {$stationId}"
        ];
        $app->halt(404, Core::output($app, $output));
    }
    echo Core::output($app, $show);
});

$app->get('/shows/:showId', function($showId) use ($app) {
    $showModel = new Show();

    $show = $showModel->oneFromShowId($showId);
    if ($show === false) {
        $output = [
        'error' => 'not found',
        'error_description' => "could not find show with {$friendlyName}"
        ];
        $app->halt(404, Core::output($app, $output));
    }
    echo Core::output($app, $show);
});
