<?php

use lib\Core;

$app->error(function (Exception $e) use ($app) {
    $app->response->setStatus(500);

    $out = ['error' => $e->getMessage()];
    echo Core::output($app, $out);
});

$app->notFound(function () use ($app) {
    $out = ['error' => 'not_found'];
    $app->halt(404, Core::output($app, $out));
});
