<?php

use lib\Core;
use models\Traffic;


$app->get('/traffic/:distance/lat/:lat/lon/:lon', function($distance, $lat, $lon) use ($app) {
    $trafficModel = new Traffic();

    $traffic = $trafficModel->allbyLocation($lon, $lat, $distance);
    if ($traffic === false) {
        $output = [
            'error' => 'not_found',
            'error_description' => "could not find traffic cams"
        ];
        $app->halt(404, Core::output($app, $output));
    }
    echo Core::output($app, $traffic);
});
