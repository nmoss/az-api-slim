<?php

use lib\Core;
use models\Highlight;

$app->get('/highlights/', function() use ($app) {
	$request = $app->request();
	$params = $request->params();
	$date = 0;
	if(array_key_exists('since', $params)) {
		$since = $params['since'];
		$date = date("Y-m-d", strtotime($since));
		//$date = date("Y-m-d", strtotime("+ " . $since . " day"));
		//$date = date('Y-m-d', strtotime('2008-02-04'));
		//$date = date('Y-m-d',(strtotime ( '-'.$since.' day' , strtotime ( $date) ) ));
		
        error_log("Page timestamp = " . $date);

    } else {

    	//$today = date('Y-m-d');
        //$date = date('Y-m-d', strtotime());
        
        //$date = date_format('Y-m-d');
        error_log(" No set date ;today's timestamp = " . $date);
    }
    $stationID = 0;
    if(array_key_exists(('stationid'), $params)) {
        $stationID = $params['stationid'];
    }

	$highlightModel = new Highlight();
	error_log("GOT HERE!!");
	$pods = $highlightModel->all($date, $stationID);
	error_log("pods = " . print_r($pods,true));

	$dateGroups = [];
    foreach ($pods as $pod) {
        $dateFormatOut = "l, F d, Y";
        $datePub = date($dateFormatOut, strtotime($pod['datepublished']));

        if (empty($pod['image'])) {
            $pod['image'] = $pod['showimage'];
        }

        $dateGroups[$datePub]['pods'][] = $pod;
    };

    $output = [];
    foreach ($dateGroups as $datePub => $dateGroup) {
        $pods = $dateGroup['pods'];

        $output[] = [
            'datepublished' => $datePub,
            'pods' => $pods
        ];
    }
	echo Core::output($app, $output);

});

$app->get('/highlights/last-date-published', function() use ($app) {

    $highlightModel = new Highlight();
    $date = $highlightModel->lastDatePublished();
    echo Core::output($app, $date);


});