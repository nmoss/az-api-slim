<?php

use lib\Core;
use models\Pod;

/**
* Output a single pod that belongs to the given audio id.
* @param int $aid The pod unique audio id associated with the desired pod.
*/
$app->get('/pods/:aid', function($aid) use ($app) {
    $podModel = new Pod();
    $pods = $podModel->oneFromAid($aid);
    echo Core::output($app, $pods);

});

/**
* Outputs all pods that belong to the given showId according to the given filter.
* @param int $showId The id associated with the desired show.
* @param array $filter Containing filter options in the form of a str array that include:
* 
*                      date : Y-m-d        return pods with the published date 
*                      ignoreaid : int     omit any pods/shows with the given ignored audio id.
*                      clip : int          1 = only returns pods that are a clip, 0 = will not return clips
*                      asc : int           result pods will be listed in ascending order (small to big) according to date.
*                                          Note: Descending by default
* 
* @example http://example.com/shows/101/pods\?filter=\=date:2008-01-23,clip:0,asc:1  Filter's subparams are separated by ',' , while
*                                                                                    each param is separated from its value by ':'.
*                   
*/             
$app->get('/shows/:showId/pods/', function( $showId ) use ($app) {
    
    $request = $app->request();
    $params = $request->params();
//
//Distinguishes Paging Property
//
    if(array_key_exists('since', $params) && is_numeric($params['since'])) {
        $since = $params['since'] + 0;
    } else {
        $since = 0;
    }
//
//Distinguishes Filter Properties 
//
    $actualFilter = [];
    if (array_key_exists('filter', $params)) {
        $filter = $params['filter'];
        $filterParts = explode(',', $filter);
        foreach ($filterParts as $filterPart) {
            $keyValueParts = explode(':', $filterPart);
            $key = $keyValueParts[0];
            $value = $keyValueParts[1];
            //Date
            if (strtolower($key) === 'date') {
                $actualFilter['date'] = date('Y-m-d',strtotime($value));
            }
            //Ignore Audio ID
            if (strtolower($key) === 'ignore_aid') {
                $actualFilter['ignore_aid'] = (int) $value;
            }
            //Clip
            if (strtolower($key) === 'clip') {
                $actualFilter['clip'] = (int) $value;
                //echo "Value for " . $key . " : ******** " . $value ;
            }
        }
    }

    $podModel = new Pod();
    $pods = $podModel->allFromShow($showId, $since, $actualFilter);
    
    $dateGroups = [];
    foreach ($pods as $pod) {
        $dateFormatOut = "l, F d, Y";
        $datePub = date($dateFormatOut, strtotime($pod['datepublished']));

        if (empty($pod['image'])) {
            $pod['image'] = $pod['showimage'];
        }

        $dateGroups[$datePub]['pods'][] = $pod;
    };

    $output = [];
    foreach ($dateGroups as $datePub => $dateGroup) {
        $pods = $dateGroup['pods'];

        $output[] = [
            'datepublished' => $datePub,
            'pods' => $pods
        ];
    }

    echo Core::output($app, $output);
});
