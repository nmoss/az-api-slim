<?php

namespace tests;

use \Slim\Slim;
use lib\Config;
use PDO;

class WebTestCase extends \PHPUnit_Framework_TestCase
{
    protected $app;
    protected $client;

    public function arraysAreSimilar($a, $b)
    {
        // if the indexes don't match, return immediately
        error_log("Comparing ". print_r($a, true)." to ".print_r($b, true));
        if (count(array_diff_assoc($a, $b))) {
            return false;
        }
        // we know that the indexes, but maybe not values, match.
        // compare the values between the two arrays
        foreach ($a as $k => $v) {
            if ($v !== $b[$k]) {
                return false;
            }
        }
        // we have identical indexes, and no unequal values
        return true;
    }

    // Run for each unit test to setup our slim app environment
    public function setup()
    {
        // Establish a local reference to the Slim app object
        
        $_SERVER['HTTP_HOST'] = 'fakehost.com';
        $pdo = $this->getMockBuilder('PDOMock')->setMethods(['prepare'])->getMock();
        Config::write('pdo', $pdo);
        $this->app = $this->getSlimInstance();
        $this->client = new WebTestClient($this->app);
    }

    // Instantiate a Slim application for use in our testing environment. You
    // will most likely override this for your own application.
    public function getSlimInstance()
    {
        return new Slim(array(
            'version' => '0.0.0',
            'debug'   => false,
            'mode'    => 'testing'
        ));
    }
}
