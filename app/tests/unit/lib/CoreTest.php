<?php

namespace tests;

use lib\Core;

class CoreTest extends LocalWebTestCase
{

    public function testOutput()
    {
        $this->client->get('/hello/Joe');
        $data = ["test" => "test"];
        $dataJson = json_encode($data);
        $output = Core::output($this->app, $data);
        $this->assertSame($output, $dataJson);
    }

    public function testPrettyOutput()
    {
        $data = ["test" => "test"];
        $dataJson = json_encode($data, JSON_PRETTY_PRINT);
        $oldRequest = $this->app->request;
        $request = $this->getMockBuilder('Slim\HTTP\Request')->disableOriginalConstructor()->getMock();
        $request->method('params')->willReturn(['pretty' => '1']);
        $this->app->request = $request;
        $output = Core::output($this->app, $data);
        $this->assertSame($output, $dataJson);
        $this->app->request = $oldRequest;
    }

    public function testStripSlashesDeep()
    {
        $data = ['testing' => 'slashes\ removal'];
        $stripped = Core::stripSlashesDeep($data);
        $this->assertSame(['testing' => 'slashes removal'], $stripped);

        $jsonData = json_encode($data);
        $decodedObject = json_decode($jsonData);
        $strippedObject = Core::stripSlashesDeep($decodedObject);
        $this->assertSame($strippedObject->testing, 'slashes removal');
    }
}
