<?php

namespace tests;

class HelloTest extends LocalWebTestCase
{
    public function testSayHello()
    {
        $this->client->get('/hello/Joe');
        $this->assertEquals(200, $this->client->response->status());
        $this->assertSame('Hello, Joe', $this->client->response->body());
    }
}
