<?php

namespace tests;

//settings to make all errors obvious during testing
error_reporting(-1);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
date_default_timezone_set('UTC');

define('PROJECT_ROOT', realpath(__DIR__ . '/..'));

require_once PROJECT_ROOT . '/vendor/autoload.php';

use \Slim\Slim;

class LocalWebTestCase extends WebTestCase
{

    public function buildFakeFetchPDO($returnData, $execute = true)
    {
        $statement = $this->getMockBuilder('stdClass')->setMethods(['bindParam', 'execute', 'fetch', 'fetchAll'])->getMock();
        $statement->expects($this->any())->method('execute')->willReturn($execute);
        $statement->expects($this->any())->method('bindParam');
        $statement->expects($this->any())->method('fetch')->willReturn($returnData);
        $statement->expects($this->any())->method('fetchAll')->willReturn($returnData);
        $pdo = $this->getMockBuilder('stdClass')->setMethods(['prepare'])->getMock();
        $pdo->expects($this->any())->method('prepare')->willReturn($statement);
        return $pdo;
    }
    
    public function getSlimInstance()
    {
        $app = new \Slim\Slim();

        // Include our core application file
        require PROJECT_ROOT . '/app.php';
        return $app;
    }
}
