<?php
require_once '../app/vendor/autoload.php';

use lib\Config;

$app = new \Slim\Slim(array('debug' => true));

require_once '../app/db.php';

try {
    $pdo = new PDO(Config::read('db.dsn'), Config::read('db.user'), Config::read('db.pass'));
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
    //$pdo->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES utf8");
    Config::write('pdo', $pdo);
} catch (Exception $e) {
    http_response_code(500);
    echo json_encode(['error' => true, 'message' => $e->getMessage()]);
    die();
}

require_once __DIR__ . '/../app/app.php';

$app->run();
